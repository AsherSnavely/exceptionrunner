1. because the logger to file is set to catch things of the INFO and FINER level
2. it comes from org.junit.jupiter.engine.* by the default settings of the engine
3. it states this function should throw an exception
4.	a. it is a variable that is related to a class and verifies that a sender and receiver of an object have/expect the same class
	b. you can not override constructors, constructors are tied to their class. That's why you had to redeclare one for the child even though all it did was call the constructor of the super with the same variable
	c. there is no need to override the other methods because the child class also has them and can be called just as an instance of the super class would call them
5. it's called whenever the class is loaded
6. it's using markdown, you can add emojis and lists can be enumerated with only the number 1. for example (which is weird)
7. in timer timeNow could not be initialized if an exception was thrown in the try branch and then when it would be used in the finally branch timeNow would still be null
	to fix it i initialized it at its declaration
8. so it seems after NullPointerException is initially thrown it is caught by something which throws  InvocationTargetException
	then NullPointerException is thrown 5 times
	which is finally caught by the ThrowableCollector of JUnit
	ThrowableCollector once again throws InvocationTargetException
	then AssertionFailedError is thrown 4 times because ThrowableCollector was expecting our TimerException not a NullPointerException
	then that is caught by something which for the last time throws InvocationTargetException
	finally NullPointerException is thrown back 4 times and the program ends
9. NullPointerException extends RuntimeException which extends Exception and is unchecked
	TimerException extends Exception and is checked